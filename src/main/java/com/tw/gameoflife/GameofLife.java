package com.tw.gameoflife;

class GameofLife {

    public final static int DEAD = 0x00;
    public final static int LIVE = 0x01;
    public final static int noOfRows = 5;
    public final static int noOfColumns = 5;
    private final int noOfIterations;
    private int[][] board = {{DEAD, DEAD, DEAD, DEAD, DEAD},
            {DEAD, DEAD, DEAD, DEAD, DEAD},
            {DEAD, DEAD, DEAD, DEAD, DEAD},
            {DEAD, DEAD, DEAD, DEAD, DEAD},
            {DEAD, DEAD, DEAD, DEAD, DEAD}};

    GameofLife(int noOfIterations, int[][] aliveCells) {
        this.noOfIterations = noOfIterations;
        for (int[] aliveCell : aliveCells) {
            int x = aliveCell[0];
            int y = aliveCell[1];
            board[x][y] = LIVE;
        }
    }

    public void start() {
        for (int i = 0; i < noOfIterations; i++) {
            board = getNextBoard();
        }
    }

    private int[][] getNextBoard() {

        if (board.length == 0 || board[0].length == 0) {
            throw new IllegalArgumentException("Board must have a positive amount of rows and/or columns");
        }

        int[][] buf = new int[noOfRows][noOfColumns];

        for (int row = 0; row < noOfRows; row++) {

            for (int col = 0; col < noOfColumns; col++) {
                buf[row][col] = getNewCellState(board[row][col], getLiveNeighbours(row, col, board));
            }
        }
        return buf;
    }

    private int getLiveNeighbours(int cellRow, int cellCol, int[][] board) {

        int liveNeighbours = 0;
        int rowEnd = Math.min(board.length, cellRow + 2);
        int colEnd = Math.min(board[0].length, cellCol + 2);

        for (int row = Math.max(0, cellRow - 1); row < rowEnd; row++) {
            for (int col = Math.max(0, cellCol - 1); col < colEnd; col++) {
                if ((row != cellRow || col != cellCol) && board[row][col] == LIVE) {
                    liveNeighbours++;
                }
            }
        }
        return liveNeighbours;
    }

    private int getNewCellState(int currentState, int liveNeighbours) {

        int newState = currentState;

        switch (currentState) {
            case LIVE:
                if (liveNeighbours < 2) {
                    newState = DEAD;
                }
                if (liveNeighbours > 3) {
                    newState = DEAD;
                }
                break;

            case DEAD:
                if (liveNeighbours == 3) {
                    newState = LIVE;
                }
                break;

            default:
                throw new IllegalArgumentException("State of cell must be either LIVE or DEAD");
        }
        return newState;
    }

    public int[][] getAliveCoordinates() {
        int[][] liveCoordinates = new int[10][2];
        int noOfLiveCoordinates = 0;
        for (int row = 0; row < noOfRows; row++) {

            for (int col = 0; col < noOfColumns; col++) {
                if (board[row][col] == LIVE) {
                    liveCoordinates[noOfLiveCoordinates][0] = row;
                    liveCoordinates[noOfLiveCoordinates][1] = col;
                    noOfLiveCoordinates++;
                }
            }
        }
        return liveCoordinates;
    }
}