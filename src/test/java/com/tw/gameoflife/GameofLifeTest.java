package com.tw.gameoflife;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class GameofLifeTest {

    @Test
    void checkBlockPatternStillLife() {
        int[][] liveCoordinates = {{1, 1}, {1, 2}, {2, 1}, {2, 2}};
        int[][] expectedLiveCoordinates = {{1, 1}, {1, 2}, {2, 1}, {2, 2}};
        GameofLife game = new GameofLife(1, liveCoordinates);

        game.start();
        int[][] resultLiveCoordinates = game.getAliveCoordinates();

        assertTwo2DArrays(expectedLiveCoordinates, resultLiveCoordinates);
    }

    @Test
    void checkBoatPatternStillLife() {
        int[][] liveCoordinates = {{0, 1}, {1, 0}, {2, 1}, {0, 2}, {1, 2}};
        int[][] expectedLiveCoordinates = {{0, 1}, {0, 2}, {1, 0}, {1, 2}, {2, 1}};
        GameofLife game = new GameofLife(1, liveCoordinates);

        game.start();
        int[][] resultLiveCoordinates = game.getAliveCoordinates();

        assertTwo2DArrays(expectedLiveCoordinates, resultLiveCoordinates);
    }

    @Test
    void checkBlinkerPatternOscillator() {
        int[][] liveCoordinates = {{1, 1}, {1, 0}, {1, 2}};
        int[][] expectedLiveCoordinates = {{0, 1}, {1, 1}, {2, 1}};
        GameofLife game = new GameofLife(1, liveCoordinates);

        game.start();
        int[][] resultLiveCoordinates = game.getAliveCoordinates();

        assertTwo2DArrays(expectedLiveCoordinates, resultLiveCoordinates);
    }

    @Test
    void checkToadPatternTwoPhaseOscillator() {
        int[][] liveCoordinates = {{1, 1}, {1, 2}, {1, 3}, {2, 2}, {2, 3}, {2, 4}};
        int[][] expectedLiveCoordinates = {{0, 2}, {1, 1}, {1, 4}, {2, 1}, {2, 4}, {3, 3}};
        GameofLife game = new GameofLife(1, liveCoordinates);

        game.start();
        int[][] resultLiveCoordinates = game.getAliveCoordinates();

        assertTwo2DArrays(expectedLiveCoordinates, resultLiveCoordinates);
    }

    private void assertTwo2DArrays(int[][] expectedLiveCoordinates, int[][] actualLiveCoordinates) {
        for (int i = 0; i < expectedLiveCoordinates.length; i++)
            assertArrayEquals(expectedLiveCoordinates[i], actualLiveCoordinates[i]);
    }
}
